##########################################################
# items_and_comments.py - (C) 2013 Daniel Fairhead
# Part of the 'basic-knowledge' Knowledge Base software
# -------------------------------------------------------
# All the CRUD of knowledge items (files)
# and comments & related media thereof.

from os.path import exists, isfile, isdir, join as pathjoin, splitext, sep
from glob import glob

from useful import readfile_with_json_or_yaml_header
import yaml

def load_file(filename):
    if isfile(filename):
       content, metadata = readfile_with_json_or_yaml_header(filename)
    else:
        content, metadata = 'New Entry', {}

def write_file(filename, content, context):
    with open(filename,'w') as f:
        f.write('---')
        f.write(yaml.safe_dump(context))
        f.write('---')
        f.write(content)

#########################################################
# Comments

'''
comments are stored as flat files, with file names:

0.markdown
1.markdown
1-0.markdown
1-1.markdown
2.markdown

etc, the dashes separate levels of comments, so it's really easy
to see the hierarchy.
'''

def commentsdir(postfile):
    return splitext(postfile)[0] + '.comments' + sep

def comments(postfile):
    return glob(commentsdir(postfile)+'*.markdown')

def add_comment(postfile, inreplyto, content, context):
    where = commentsdir(postfile)

    if not isdir(where):
        mkdir(where)

    parentpost = pathjoin(where, inreplyto)
    new_comment_file = new_replyfile(parentpost)

    write_file(new_comment_file, content, context)

    return new_comment_file

def new_replyfile(prefix):
    ''' given a filename $prefix "filename", returns the next available comment number '''

    current_highest = -1
    if prefix[-1] != sep:
        # We're looking at subcomments, so use a - deliminator
        #        prefix += '-'

        if prefix.endswith('.markdown'):
            prefix = prefix[:-9]
        prefix += '-'

    prefix_len = len(prefix) # don't recalculate each time...

    for filename in glob(prefix + '*.markdown'):
        suffix = filename[prefix_len:]
        if '-' not in suffix: # if there is, then it's a sub-comment. ignore it.
            try:
                num = int(suffix.split('.')[0])
                if num > current_highest:
                    current_highest = num
            except ValueError as e:
                pass
    return '{0}{1}.markdown'.format(prefix, str(current_highest+1).zfill(3))




def load_comments(filename):
    comments = []

    if isdir(filename):
        # TODO: check glob is sorting comments correctly (w/ replies)
        for comment in glob(pathjoin(commentsdir(filename),'*.markdown')):
            comments.append(readfile_with_json_or_yaml_header(comment))
            comments[-1]['level'] = filename.count('-')


