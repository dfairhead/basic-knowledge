'''
    items.py (C) 2013 Daniel Fairhead
    part of the 'Basic Knowledge' Knowledge Base system

    Turns lib.items_and_comments into specific functions
    for this 'flat file database'. this should have no reference to
    the app - although it's a bit of a pain to use it without.
    it's totally decoupled. the views, etc, should
    have no logic and complex stuff in them. That should all be
    in the app/logic.py, which then is a sanity wrapper for the actual
    beef which is here, and in cache.py, and o on.

    Most of the item stuff is packaged up in the Item class, which
    you use with a 'with' format (partly as I'm addicted to this pattern
    right now) which then allows all caches to be updated easily when
    you close the with block:

    >>> with Item(DB_CACHEFILE, '/var/data-root', 'index') as item:
            contents, metadata = item.get()

    which works fine. or:

    >>> with Item(DB_CACHEFILE, '/var/data-root', 'index') as item:
            item.update(TEXT, metadata)
            item.add_comment('blah!', comment_metas)

    this is a bit wordy (having to specify the DB_CACHEFILE and data root,
    so normally you should create an application specific wrapper which
    loads them from the config file, and holds them.

    >>> class BKItem(Item):
            def __init__(self, key):
                super(BKItem, self).__init__(CACHEFILE, DATA_ROOT, key)

'''

from itertools import islice
import sqlite3 as lite

from cache import Cache

#####################################################################
# Useful helper functions: (could be split out I guess...)

def safe_filename(unsafe):
    # TODO!!!!!!
    return unsafe

#####################################################################
# Main Item Class:

class Item(object):
    changed = False

    def __init__(self, db_cachefile, data_root, unsafe_item_id):
        self.db_cachefile = db_cachefile
        self.data_root = data_root

        self.item_id = safe_filename(unsafe_item_id)

        basepath = pathjoin(abspath(data_root), self.item_id) 
        self.filename =  basepath + '.markdown'
        self.commentsdir = basepath + '.comments/'
        self.mediadir = basepath + '.media/'

    def __enter__(self):
        ''' Called with the 'with' pattern. '''
        # TODO: ?what do we do?
        pass

    def __exit__(self, exptype, expvalue, exptb):
        ''' end of 'with' pattern. updates database cache. '''
        # TODO:
        if self.changed:
            # update cachefile(s)
            pass
        pass

    #####################################################################
    # item CRUD:


    def get(self):
        # TODO
        return 'content', {}

    def update(self, content, context):
        self.changed = True
        # TODO
        pass

    def rename(self, new_name):
        self.changed = True
        # This could be a little complicated...
        # TODO:
        pass

    def delete(self):
        self.changed = True
        # TODO
        pass

    #####################################################################
    # media CRUD:
    # the get_media gets *all* media as a list.  There's no need
    # for individual media entities to be 'got'.

    def get_media(self):
        # TODO
        return ['list','of','media','files?', \
                'and','how','to','access','them']

    def add_media(self, new_media_name):
        self.changed = True
        # TODO: also update thumbnails? here or in the end of the with?
        # TODO
        pass

    def rename_media(self, old_name, new_name):
        pass

    def del_media(self, media_name):
        self.changed = True
        # TODO
        pass

    #####################################################################
    # comments CRUD:

    def get_comments(self):
        # TODO
        # returns dict ready for jsonifying?  Or is iter ready to be streamed
        # through json???
        pass

    def add_comment(self, content, metadata, parent=None):
        self.changed = True
        # TODO
        pass

    def del_comment(self, comment_id):
        self.changed = True
        # TODO
        pass
