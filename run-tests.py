#!.virtualenv/bin/python
# -*- coding: utf-8 -*-

import unittest
import sys
import logging

reload(sys)
sys.setdefaultencoding('utf-8')

#import tests.logic_tests
import tests.test_pagestore

pagestore = unittest.TestLoader().loadTestsFromModule(tests.test_pagestore)

unittest.TextTestRunner(verbosity=1).run(pagestore)
