'''
    logic.py (C) 2013 Daniel Fairhead
    part of the 'Basic Knowledge' Knowledge Base system

    Turns lib.items_and_comments into specific functions
    for this app. items_and_comments should have no reference
    to the app, it's totally decoupled. the views, etc, should
    have no logic and complex stuff in them. That should all be
    here.

    Most of the item stuff is packaged up in the Item class, which
    you use with a 'with' format (partly as I'm addicted to this pattern
    right now) which then allows all caches to be updated easily when
    you close the with block:

    >>> with Item('index') as item:
            contents, metadata = item.get()

    which works fine. or:

    >>> with Item('index') as item:
            item.update(TEXT, metadata)
            item.add_comment('blah!', comment_metas)

    only updates the searchable cache once, rather than once on update
    and once for adding the comment.

'''

from itertools import islice
import sqlite3 as lite

from app import app

#####################################################################
# Useful helper functions: (could be split out I guess...)

def safe_filename(unsafe):
    # TODO!!!!!!
    return unsafe

###############
# TODO: database class, for storing cache, and searching. - could be split out.

class Cache(object):
    changed = False

    def __init__(self):
        self.db = lite.connect(app.CACHE_DB_NAME)
        self.cur = self.db.cursor()


    def initialise(self):
        self.changed = True

        # does virtual table (with FTS searching, etc) exist?
        self.cur.execute(
            u'SELECT DISTINCT tbl_name FROM sqlite_master WHERE tbl_name = "itemcache"')
        # TODO: check for result...
        if not result:
            self.cur.execute( u"CREATE VIRTUAL TABLE 'itemcache' USING fts4(" \
                              u"title, content, metadata, comments, media)")

            self.cur.execute( u"CREATE TABLE tags(tag, title)" )

    def __enter__(self):
        ''' called with the 'with' pattern. '''
        pass

    def __exit__(self, exptype, expvalue, exptb):
        if self.changed:
            self.db.commit()

        self.db.close()
        ''' exit the 'with' pattern. close the db. '''
        pass

    def all_items(self):
        # TODO
        return ['list','of','items']

    def search(self, q):
        # TODO
        return ['list','of','items']

    def purge(self, item_id=False, everything=False):
        self.changed = True

        if item_id:
            self.cur.execute ( u"DELETE FROM 'itemcache' WHERE title == ?", item_id)
            self.cur.execute ( u"DELETE FROM 'tags' WHERE title == ?", item_id)

        if everything:
            self.cur.execute( u"DROP TABLE IF EXISTS 'itemcache'")
            self.cur.execute( u"DROP TABLE IF EXISTS 'tags'")
            self.initialise()

    def update(self, item_id, content, meta_data, comments, media):
        self.changed = True
        # TODO
        pass

#####################################################################
# Main Item Class:

class Item(object):
    changed = False

    def __init__(self, unsafe_item_id):
        self.item_id = safe_filename(unsafe_item_id)

    def __enter__(self):
        ''' Called with the 'with' pattern. '''
        # TODO: ?what do we do?
        pass

    def __exit__(self, exptype, expvalue, exptb):
        ''' end of 'with' pattern. updates database cache. '''
        # TODO:
        if self.changed:
            # update cachefile(s)
            pass
        pass

    #####################################################################
    # item CRUD:


    def get(self):
        # TODO
        return 'content', {}

    def update(self, content, context):
        self.changed = True
        # TODO
        pass

    def rename(self, new_name):
        self.changed = True
        # This could be a little complicated...
        # TODO:
        pass

    def delete(self):
        self.changed = True
        # TODO
        pass

    #####################################################################
    # media CRUD:
    # the get_media gets *all* media as a list.  There's no need
    # for individual media entities to be 'got'.

    def get_media(self):
        # TODO
        return ['list','of','media','files?', \
                'and','how','to','access','them']

    def add_media(self, new_media_name):
        self.changed = True
        # TODO: also update thumbnails? here or in the end of the with?
        # TODO
        pass

    def rename_media(self, old_name, new_name):
        pass

    def del_media(self, media_name):
        self.changed = True
        # TODO
        pass

    #####################################################################
    # comments CRUD:

    def get_comments(self):
        # TODO
        # returns dict ready for jsonifying?  Or is iter ready to be streamed
        # through json???
        pass

    def add_comment(self, content, metadata, parent=None):
        self.changed = True
        # TODO
        pass

    def del_comment(self, comment_id):
        self.changed = True
        # TODO
        pass
