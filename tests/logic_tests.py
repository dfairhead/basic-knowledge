from lib.items_and_comments import load_file, write_file, commentsdir, comments, add_comment, new_replyfile

from glob import glob

DATA_DIR = 'data-root/'

print 'list of files:'
files = glob(DATA_DIR + '*.markdown')
print files
print '-----------------------------------------'

servers = files[0]
index = files[1]

def post_info(filename):
    print ' Post: -  ', filename
    print ' com dir  ', commentsdir(filename)
    print ' comments ', comments(filename)
    print '    next: ', new_replyfile(commentsdir(filename))
    #if len(comments(filename)) > 0:
    #if True:
        #print ' subnext: ', new_replyfile(filename)
        #add_comment(filename,'','Comment text.',{'title':'temp'})

post_info(servers)

#post_info(index)
