# Basic Knowledge
A very simple web (flask/python) knowledge base for small teams.
Very much Work in Progress.
*Don't try to use yet!*

## Design concept

Flat files, easily backed up and version controlled with git or
whatever you prefer. Thus multiple instances of the data can be
synched easily without complex database merge crap.  Let git handle it.  Cached with sqlite.  Single page app, ajax
stuff.  Lightweight, and should be mobile & desktop happy.
Preferably offline items can be cached and synched when needed.

### items

Each 'item' of content should be reasonably short, and stored in a .markdown file (in a main data directory).

- index.markdown
- server-config.markdown
- nginx-options.markdown

etc.

Each of these are categoriesed by a normal tags based system.  The tags are stored (along with any other metadata) with a usual jekyll 'frontmatter' yaml block:

    ---
    title: Final Cut Pro Plugin Reset
    tags: [editors, FCP, bugs, plugins]
    ---
    Edit 2 stopped loading FCP, somehow the plugin dir got
    messed up. no idea how.  Reset as follows:
    ...

### media

Any media files which are needed for the item are stored in a
folder of the same name (without the .markdown):

- index.markdown
- *index.media/*
- - logo.jpg
- server-config.markdown
- *server-config.media/*
- - drawing.svg
- - initial-load.jpg

etc.

### comments

Comments can be left on any item, and are stored in a comments
directory, numerically named, with a simple hierarchy:

- server-config.markdown
- *server-config.media/*
- - drawing.svg
- - initial-load.jpg
- *server-config.comments/*
- - 000.markdown
- - 001.markdown
- - 001-000.markdown
- - 001-001.markdown
- - 002.markdown

etc.  This allows very fast simple comment generation, sorting, and so on with hardly any work at all.  All this gets cached to sqlite for speed of loading, and as each file is easily separate, should have very little cause of git conflicts.

### security thoughts.

I've thought about this a lot... Either we do security the
normal way, with our own user database & hashing and all that
crap, or else a simpler (for the user) system. You log in with
an email address & no password.  The system them emails you a
public key which you paste into your browser and gets stored
locally as a cookie...  I dunno.  When I thought of the idea it
sounded simple, but now it seems quite complex. :-)

This system is *not* designed for being available on the web,
and is *not* intended for massive teams where you need user
hierarchies and permissions and all that.  Of course, it should
all be backed up and version controlled with git, but I don't
want it to feel complex at all to use.

We could just track ip address - but this is very open to abuse.

## Usage:

    ./setup.sh

will download and setup virtualenv and all the required modules in .virtualenv/

    ./run.py

runs it all, in flask development mode.
For production deployment, look at the flask documentation.

## More info:

This is using my usual flask bootstrap setup:

The virtual env is kept in .virtualenv, and usually shouldn't need to be touched.  I don't like the entering and exiting a virtualenv business, so went with the 'virtualenv stuff happens transparently when you use run.py, you shouldn't have to care about it'.  If you want to run python for the virtualenv, use .virtualenv/bin/python

The setup.sh script is to allow you to get up an running on a new machine in seconds. (On an old machine, minutes, while it downloads and installs flask...)  I like the github idea of 'all a new developper needs to do is run one command, and they have a whole system ready to go'.

## pre-commit hook
There is the [pre-commit script by Sebastian Dahlgren](https://github.com/sebdah/git-pylint-commit-hook) in the .setup/hooks/ folder, which will run pylint on python scripts to check they are valid before you commit them. The setup.sh script will copy this into your .git/hooks by default.

To run a git commit *without* using this, use:

    git commit --no-verify
